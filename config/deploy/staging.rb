role :app, 'deploy@ip_address:port'
role :web, 'deploy@ip_address:port'
role :db,  'ip_address'

set :rails_env, :staging
